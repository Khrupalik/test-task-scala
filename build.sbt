name := "TestTask"

version := "1.0"

scalaVersion := "2.11.7"

resolvers += "Spray" at "http://repo.spray.io"

libraryDependencies ++= Seq(
  "io.spray"    %% "spray-can"        % "1.3.1",
  "io.spray"    %% "spray-http"       % "1.3.1",
  "io.spray"    %% "spray-routing"    % "1.3.1",
  "io.spray"    %% "spray-client"     % "1.3.1",
  "io.spray"    %% "spray-testkit"    % "1.3.1"   % "test",
  "io.spray"    %% "spray-json"       % "1.2.6"
)
libraryDependencies += "org.scalaz.stream" % "scalaz-stream_2.10" % "0.8"
libraryDependencies += "com.typesafe.akka" % "akka-actor_2.11" % "2.4.0"

//libraryDependencies += "org.specs2" % "specs2_2.9.1" % "1.8.1"
libraryDependencies += "org.specs2" % "specs2-core_2.10" % "3.6-scalaz-7.0.7"
