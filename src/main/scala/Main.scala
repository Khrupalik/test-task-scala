import akka.actor.Actor.Receive
import akka.actor.{Props, Actor, ActorSystem}
import akka.io.IO
import spray.can.Http
import spray.http.{MediaTypes, MediaType}
import spray.httpx.marshalling._
import spray.routing.HttpService
import akka.actor.{ActorSystem, Props}
import akka.io.IO
import spray.can.Http
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import scala.xml.Elem

/**
 * Created by root on 13.11.15.
 */
object Main extends App {
  implicit val system = ActorSystem("on-spray-can")

  val service = system.actorOf(Props[ServiceActor], "demo-service")

  implicit val timeout = Timeout(5.seconds)

  IO(Http) ? Http.Bind(service, interface = "localhost", port = 8080)
}

class ServiceActor extends Actor with MyService {

  def actorRefFactory = context

  def receive = runRoute(myRoute)
}