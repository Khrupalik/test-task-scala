import spray.http.MediaType
import spray.routing.HttpService

import scala.xml.Elem

/**
 * Created by root on 14.11.15.
 */
trait MyService extends HttpService with CalculationComponentImpl {

  this: CalculationComponent =>

  val myRoute =
    path("api") {
      get {
        ctx => {

            ctx.complete {
              getV1(scala.xml.XML.loadString(ctx.request.entity.data.asString)) match {
                case Some(v1) =>
                  val element = calculationModel.getValue(v1)


                  element match {
                    case Right(res) =>
                      <response>
                        <status>0</status>
                        <result>
                          {res}
                        </result>
                      </response>
                    case Left(err) =>
                      <response>
                        <status>1</status>
                        <result>
                          {err}
                        </result>
                      </response>
                  }
                case None =>
                  <response>
                    <status>2</status> <result>Value not found!</result>
                  </response>
              }

          }
        }
      } ~ post {
        ctx => {
          ctx.complete {

            toObj(scala.xml.XML.loadString(ctx.request.entity.data.asString)) match {
              case Some((v2, v3, v4)) =>
                val element = calculationModel.calculate(v2, v3, v4)

                element match {
                  case Right(res) =>
                    <response>
                      <status>0</status>
                      <result>
                        {if (res) 1 else 0}
                      </result>
                    </response>
                  case Left(err) =>
                    <response>
                      <status>1</status>
                      <result>
                        {err}
                      </result>
                    </response>

                }
              case None =>
                <response>
                  <status>2</status> <result>Value not found!</result>
                </response>
            }
            //            println(ctx.request.entity.data.asString)

          }
        }
      }

    }

  def toObj(xml: Elem) = {
    for {
      v2 <- (xml \\ "v2" headOption).map(_.text.toInt)
      v3 <- (xml \\ "v3" headOption).map(_.text.toInt)
      v4 <- (xml \\ "v4" headOption).map(_.text.toInt)
    } yield {
      (v2, v3, v4)
    }
  }

  def getV1(xml: Elem): Option[Int] = {
    for {
      v1 <- (xml \\ "v1" headOption).map(_.text.toInt)
    } yield {
      v1
    }
  }
}
