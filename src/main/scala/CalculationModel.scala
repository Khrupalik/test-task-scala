import java.io.{PrintWriter, File}
import scala.collection.JavaConversions._
import scala.util.Try

/**
 * Created by root on 13.11.15.
 */
trait CalculationComponent {

  def calculationModel: CalculationModel

  trait CalculationModel {
    def getValue(va: Int): Either[String, Int]

    def calculate(v2: Int, v3: Int, v4: Int): Either[String, Boolean]
  }

}


trait CalculationComponentImpl extends CalculationComponent {

  lazy val calculationModel = new CalculationModelImpl

  class CalculationModelImpl extends CalculationModel {



    override def getValue(v1: Int): Either[String, Int] = handle {

      val result = for (value <- FileUtils.getElement(FileType.TEMP, v1)) yield {

        if (value > v1)
          value - 10
        else
          value
      }

      result map {
        rs =>
          Right(rs)
      } getOrElse Left("Sorry, but file no exist..")

    }


    override def calculate(v2: Int, v3: Int, v4: Int): Either[String, Boolean] = handle {

      val result = for {
        value <- FileUtils.getElement(FileType.ORIGIN, v3)
      } yield {
          val result = if (value + v2 > 10)
            value + 10 + v2
          else value + v2

          FileUtils.getLine(FileType.TEMP) foreach {
            line =>
              FileUtils.putValue(FileType.TEMP, line.patch(v4, Seq(result), 1))
          }
        }

      Right(result.isDefined)

    }

    private def handle[T](block: => Either[String, T]): Either[String, T] = Try {
      block
    } recover {
      case e =>
        Left(e.toString)
    } get
  }

}