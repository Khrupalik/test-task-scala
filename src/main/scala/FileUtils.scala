import java.io.{PrintWriter, File}

import FileType.FileType


/**
 * Created by root on 14.11.15.
 */
object FileType extends Enumeration {
  type FileType = Value
  val ORIGIN, TEMP = Value

}

object FileUtils {

  private val file1 = new File("file1")
  private val file2 = new File("file2")

  def getElement(fileT: FileType, v: Int) = {
    this.synchronized {

      val file = getFile(fileT)

      val bufferedSource = io.Source.fromFile(file)

      for (line <- bufferedSource.getLines().toSeq.headOption) yield {
        val cols = line.split(",").map(_.trim)
        cols(v).toInt
      }
    }
  }

  def getLine(fileT: FileType) = {

    this.synchronized {
      val file = getFile(fileT)

      val bufferedSource = io.Source.fromFile(file)

      for (line <- bufferedSource.getLines().toSeq.headOption) yield {
        line.split(",").map(_.trim.toInt)
      }
    }
  }

  def putValue(fileT: FileType, newLine: Seq[Int]) = {
    this.synchronized {
      val file = getFile(fileT)

      val writer = new PrintWriter(file)
      writer.write(newLine.mkString(","))
      writer.close()
    }
  }

  private def getFile(fileType: FileType) = if (fileType == FileType.ORIGIN) file1 else file2

}


